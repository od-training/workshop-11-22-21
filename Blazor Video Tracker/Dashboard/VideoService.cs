﻿using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;

namespace Blazor_Video_Tracker.Dashboard
{
    public class VideoService
    {
        readonly HttpClient _httpClient;

        public VideoService(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }
        public async Task<Video[]> GetVideos()
        {
           return await _httpClient.GetFromJsonAsync<Video[]>("videos");
        }
    }

    public class Video
    {
        public string Id { get; set; }
        public string Title {get; set;}
        public string Author {get; set;}

        public Video()
        { }

        public Video(string _id, string _title, string _author)
        {
            Id = _id;
            Title = _title;
            Author = _author;
        }



    }
}
